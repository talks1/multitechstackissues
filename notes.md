
2 stack issues:  duplication of effort
- need to keep track of multiple technology stack functionality
    - different deployment approaches
    - different coding standards
- Duplication of validation logic of information entities (need to validate in browser and again in the back end)
- no opportunity for end to end unit testing as the value chain jumps between different technologies
- enforced artifical micro services which split front end and back end logic
- requires API development between different tech stacks
- inability to shift functionality developd in back end to front end
    - example Ground Gauge Kriging Algorithm
- more setup to support rapid development as you need to watch the filesystem on the front end and back and and restart processes.  
    - With a javascript only stack there are less moving parts as the entire solution can run in the browser and hot reload there.
- can require different skills and therefore different roles for front and back end development.  This causes communication challenges to synchronize front and back end changes.
- inability to produce standalone solution which can be easily emailed to stakeholders.  A javascript solution can be easily package into a single html file to send to a stakeholder.
- technical constraints are increased as not all libraries available in one tech stack have a corresponding compatible solution in the alternate stack
- need to ensure that front end authorization is compatable with back end 